// declare all variable needed
let startDate, endDate, dateWhenAdded, pricePerDay, dayData, periodData, errorMessage, periodDaysCount, historyData;
let   table = document.getElementById('pricePeriodsData');

periodData = [];

// set default value of all date inputs
(function SetDefaultValueDates() {
    Array.from(document.querySelectorAll(['[type="date"'])).forEach(input => input.defaultValue = new Date().toJSON().split('T')[0]);
})();

//submit data

document.getElementById('priceHistory').addEventListener('submit', (event) => {
    // prevent default events executingzz
    event.preventDefault();
  
    // get all values added to the fields
    startDate = document.getElementById("startDate").value;
    endDate = document.getElementById("endDate").value;
    dateWhenAdded = document.getElementById("dateWhenAdded").value;
    pricePerDay = document.getElementById("pricePerDay").value;

    //generate object for current period data
    dayData = {
        startDate: startDate,
        endDate: endDate,
        dateWhenAdded: dateWhenAdded,
        pricePerDay: pricePerDay
    }
     generateHistoryData(dayData);
     generateTable(table, historyData);
});

function generateHistoryData(data) {
    periodData.push(data);   
     // keep history data in localStorage
     localStorage.setItem('DATA', JSON.stringify(periodData));
     historyData = JSON.parse(localStorage.getItem('DATA'));
     return periodData
}




// GENERATE TABLE

// generate table head
function generateTableHead(table, data) {
    let thead = table.createTHead();
    if(!thead.hasChildNodes()) {
         let tr = thead.insertRow();
         if (data !== null || data !== undefined) {
            Object.keys(data[0]).forEach(element => {
                    let text = document.createTextNode(element),
                        th = document.createElement('th');
                        th.appendChild(text);
                        thead.appendChild(tr);
                        tr.appendChild(th);
                       
            
                });
                
       } else {
           return false;
       }
    }  
}
// generate table cells

function generateTableCells(table, data) {
    let tbody = table.createTBody();

    for (let element of data) {
      let row = table.insertRow();
      for (key in element) {
        let cell = row.insertCell(),
        text = document.createTextNode(element[key]);
        cell.appendChild(text);
        tbody.appendChild(row)
        row.appendChild(cell)
      }
  
    }
    if (tbody.previousElementSibling.tagName === 'TBODY' ) {
        console.log(tbody.previousElementSibling)
        tbody.previousElementSibling.remove()
    }
  }

function generateTable(table, data) {
    generateTableHead(table, data)
    generateTableCells(table, data)
}

let submitted = false;
// SUBMIT FORM FOR CALCULATING TOTAL PRICE
document.getElementById('getTotalForPeriods').addEventListener('submit', (event) => {
    event.preventDefault();
    submitted = true;
    // get all values added to the fields
    selectedStartDate = document.getElementById("selectedStartDate").value;
    selectedEndDate = document.getElementById("selectedEndDate").value;

    // fill list info
    document.getElementById('info-startDate').innerHTML = selectedStartDate;
    document.getElementById('info-endDate').innerHTML = selectedEndDate;

    let dateRanges = getDatesBetweenPeriods(new Date(selectedStartDate), new Date(selectedEndDate));

    collectRangeDates();
   
  Array.from(dateRanges).forEach(range => {
    let listWrapper = document.getElementById('info-datesBetweenPeriod');
    let list = document.createElement('li');
    listWrapper.appendChild(list);
    list.appendChild(document.createTextNode(range.toLocaleDateString('en-US')));
  })
   
  // count dates between selected period
     document.getElementById('countDates').innerHTML = countDatesInPeriod(new Date(selectedStartDate), new Date(selectedEndDate));
   
     getTotalPrice(dateRanges);
    
})

  // // get list of all days between the period
// DATE RANGES
function getDatesBetweenPeriods (startDate, endDate) {
    let dates = []
    //to avoid modifying the original date
    const theDate = new Date(startDate)
    while (theDate < endDate) {
      dates = [...dates, new Date(theDate)]
      theDate.setDate(theDate.getDate() + 1)
    }
    dates = [...dates, endDate]
    return dates
 }

// count dates in period
function countDatesInPeriod(startDate, endDate) {
    return (new Date(endDate) - new Date(startDate))/(1000*60*60*24) +1 
}

function collectRangeDates() {
    historyData.forEach(period => {
       let dateRangesPeriod = getDatesBetweenPeriods(new Date(period.startDate), new Date(period.endDate)),
       converteddateRangesPeriod = dateRangesPeriod.map((period) => new Date(period).toISOString().split('T')[0]);
       period.datesBetweenRange = converteddateRangesPeriod;
      
    })
}

function getTotalPrice(dateRanges) {
    console.log(historyData)
    historyData.reduce(function(totalPrice, currentPeriod, i, array) {
        let getDatesBetweenPeriod = [...currentPeriod.datesBetweenRange]
        // get previous period
        let previousPeriod = array[ i==0?array.length-1:i-1],

        // get all dates selected 
        selectedDateCollection = getDatesBetweenPeriod.map((period) => new Date(period).toISOString().split('T')[0]),
        
        // set all days between periods
        allDays = [];

        // getAll dates in periods matched the selected period dates
         let allDatesInRange =  [...previousPeriod.datesBetweenRange, ...currentPeriod.datesBetweenRange];
     
            // get price for later data overlap
            allDays = allDatesInRange.filter((c, index) => {
                return allDatesInRange.indexOf(c) === index;
            });

            // check if selected period dates match to some date in all periods 
            let hasMatches =  allDays.some(date => {
            console.log('date: ', date)
            return selectedDateCollection.indexOf(date) != -1
        });
        
            // get overlapped dates
            let overLappedDays = allDatesInRange.filter((c, index) => {
                return allDatesInRange.indexOf(c) !== index;
            });

         if(hasMatches && submitted) {
            // get  total price when there is overlapped dates between dates in periods
            totalPrice = (previousPeriod.datesBetweenRange.length 
                - overLappedDays.length)*previousPeriod.pricePerDay 
                + overLappedDays.length*currentPeriod.pricePerDay
                
                if(dateRanges.length > allDays.length ) {
                    totalPrice += (dateRanges.length - allDays.length)*5
                }
        } else if(!hasMatches && submitted) {
             // get  total price when there is overlapped dates between dates in periods
            console.log('getDatesBetweenPeriod',getDatesBetweenPeriod)
           totalPrice = dateRanges.length * 5;
        }
         console.log('totalPrice', totalPrice)
    return totalPrice
    }, 0) 
}