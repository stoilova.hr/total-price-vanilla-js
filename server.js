const express = require('express');
const path = require('path');
const open = require('open');

const port = 8001;


const connectLivereload = require("connect-livereload");
const app = express();

// set the view engine from html to ejs
app.set('view engine', 'ejs');

app.use(connectLivereload());

const livereload = require("livereload");

const liveReloadServer = livereload.createServer();
liveReloadServer.watch(path.join(__dirname, './views'));

liveReloadServer.server.once("connection", () => {
    setTimeout(() => {
      liveReloadServer.refresh("/");
    }, 100);
  });

app.use(express.static(__dirname + '/'));

app.get('/', function(req, res) {
  res.render('pages/index');
});


//   app.listen(port, function(err) {
//     if (err) {
//         console.log(err);
//     } else {
//         open('http://localhost:' + port);
//     }
// });

app.listen(port)